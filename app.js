var express = require('express'),
  app = express(),
  server = require('http').createServer(app),
  io = require('socket.io').listen(server),
  users = {},
  port = process.env.PORT || 3000;

server.listen(port);

app.get('/', function(req, res){
  res.sendfile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket){
  socket.on('new user', function(data, callback){
    if (data in users){
      callback(false);
    } else{
      callback(true);
      socket.username = data;
      users[socket.username] = socket;
      updateUsernames();
    }
  });
  
  function updateUsernames(){
    io.sockets.emit('usernames', Object.keys(users));
  }

  socket.on('send message', function(data, callback){
    var msg = data.trim();
    console.log(socket.username + ': ' + msg); // output conversation in terminal
    if(msg.substr(0,3) === '/w '){
      msg = msg.substr(3);
      var ind = msg.indexOf(' ');
    } else{
      io.sockets.emit('new message', {msg: msg, user: socket.username});
    }
  });
  
  socket.on('disconnect', function(data){
    if(!socket.username) return;
    delete users[socket.username];
    updateUsernames();
  });
});