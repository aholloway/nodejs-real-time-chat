# Real-time chat

A running version is available on Heroku at [http://nodejs-real-time-chat.herokuapp.com](http://nodejs-real-time-chat.herokuapp.com)

## Requirements
* NodeJS
* ExpressJS
* Socket.IO

## Installation
* Install [NodeJS](http://nodejs.org/download/)
* Run `npm install` to install all the packages listed in package.json
* In a terminal window, go into the project root where server.js is and run `node app.js`
* Open up two browser windows and go to `http://127.0.0.1:3000/`
* Enter your name in each browser window and chat away!